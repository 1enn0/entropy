#! /usr/bin/env ipython

import re
import os
import sys

# set full_search to 1 if all memory regions in the maps
# should be scanned
full_search = False

if not os.path.isdir('memdumps'):
    os.mkdir('memdumps')

search_strings = [
    '3061',
    'zun bicpekuto hidu',
    'mehqadace',
    'Mrsp0HQqhVIib!IsC00_',
    'Pl47EwU3',
    'Mod5-Daj',
    'Taz4',
    'FasoBatiRegd6['
]

pid_entropy = get_ipython().getoutput('pidof entropy')
pid_entropy = pid_entropy[0]

if full_search:
    mmaps = get_ipython().getoutput('cat /proc/{pid_entropy}/maps')
else:
    mmaps = get_ipython().getoutput('cat /proc/{pid_entropy}/maps | grep heap')

mmaps = '\n'.join(mmaps)

if full_search:
    res = re.findall('([\w\d]{8,12})-([\w\d]{8,12}).+\s+(\S+)?\n', mmaps)
else:
    res = re.findall('([\w\d]{8,12})-([\w\d]{8,12}).+\s+(\S+)?', mmaps)

n_res = len(res)
dump_filenames_map = {'memdumps/dump_{}_{}'.format(s, e): v for (s, e, v) in res}
gdb_cmds = ['"dump memory memdumps/dump_{}_{} 0x{} 0x{}"'.format(s, e, s, e) for (s, e, _) in res]
gdb_cmd = ' -ex '.join(gdb_cmds)

print ('Attaching gdb to PID {} and dumping memory regions:'.format(pid_entropy))
for r in res:
    print('0x{} .. 0x{} : {}'.format(r[0], r[1], r[2]))

tmp = get_ipython().getoutput("gdb -q -p {pid_entropy} -ex {gdb_cmd} -ex 'quit'");

for dump_filename, filename in dump_filenames_map.items():
    strings_output = get_ipython().getoutput('strings -a {dump_filename}')
    strings_output = '\n'.join(strings_output)
    
    hits = []
    for pw in search_strings:
        hits.append(re.findall(re.escape(pw), strings_output))
        get_ipython().getoutput('rm {dump_filename}')

    n_hits = 0
    for h in hits:
        n_hits = n_hits + len(h) if len(h) > 0 else n_hits

    if n_hits > 0:
        print('\n{} : {}'.format(dump_filename, filename))
        for h, pw in zip(hits, search_strings):
            if (len(h) > 0):
                print ('[ {}x ] {}'.format(len(h), pw))
    

os.rmdir('memdumps')
