/* entropy-mpw.h
 *
 * Copyright 2018 Lennart Hannink
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib-object.h>

#include "libmpw/mpw-algorithm.h"
#include "libmpw/mpw-marshal.h"
#include "libmpw/mpw-types.h"
#include "libmpw/mpw-util.h"

G_BEGIN_DECLS

#define ENTROPY_TYPE_MPWOPERATION (entropy_mpw_operation_get_type())
G_DECLARE_FINAL_TYPE (EntropyMpwOperation, entropy_mpw_operation, ENTROPY, MPWOPERATION, GObject)

GObject                *entropy_mpw_new           (void);

const gchar*            entropy_mpw_get_full_name (EntropyMpwOperation *self);
void                    entropy_mpw_set_full_name (EntropyMpwOperation *self,
                                                   const gchar         *full_name);

const gchar*            entropy_mpw_get_master_pw (EntropyMpwOperation *self);
void                    entropy_mpw_set_master_pw (EntropyMpwOperation *self,
                                                   const gchar         *master_password);

const gchar*        entropy_mpw_process_identicon (const gchar *full_name,
                                                   const gchar *master_password);

const int        entropy_mpw_get_user_sites_count (EntropyMpwOperation *self);

const gchar*       entropy_mpw_get_user_site_name (EntropyMpwOperation *self,
                                                   const int            site_id);

MPMarshalledSite            *entropy_mpw_get_site (EntropyMpwOperation *self,
                                                   const guint          site_id);

const gchar*              entropy_mpw_get_site_pw (EntropyMpwOperation *self,
                                                   const int            site_id);
void                 entropy_mpw_set_site_counter (EntropyMpwOperation *self,
                                                   const int            site_id,
                                                   const int            counter);
void             entropy_mpw_set_site_result_type (EntropyMpwOperation *self,
                                                   const int site_id,
                                                   const MPResultType result_type);
void            entropy_mpw_set_site_algo_version (EntropyMpwOperation *self,
                                                   const int site_id,
                                                   const MPAlgorithmVersion algo_version);

const gchar*  entropy_mpw_get_site_pw_placeholder (EntropyMpwOperation *self,
                                                   const int            site_id);

void                          entropy_mpw_sign_in (EntropyMpwOperation *self);


G_END_DECLS
