/* entropy-app.c
 *
 * Copyright 2018 Lennart Hannink
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtk/gtk.h>

#include "entropy-app.h"
#include "entropy-window.h"

struct _EntropyApp
{
  GtkApplication parent;
};

G_DEFINE_TYPE (EntropyApp, entropy_app, GTK_TYPE_APPLICATION)

static void
entropy_app_init (EntropyApp *app)
{
}

static void
preferences_activated (GSimpleAction *action,
                       GVariant      *parameter,
                       gpointer       app)
{
}

static void
quit_activated (GSimpleAction *action,
                GVariant      *parameter,
                gpointer       user_data)
{
  EntropyApp *app = ENTROPY_APP (user_data);

  /* destroy all windows associated with our app
   * so their dispose/finalize functions will be called
   * and all senstive memory is overwritten
   */
  GList *windows;
  GList *l;
  windows = gtk_application_get_windows (GTK_APPLICATION (app));
  for (l = windows; l != NULL; l = l->next)
  {
    GtkWindow *cur_window = GTK_WINDOW (l->data);
    if (ENTROPY_IS_WINDOW (cur_window))
    {
      gtk_widget_destroy (GTK_WIDGET (cur_window));
    } 
  }

  g_application_quit (G_APPLICATION (app));
}

GActionEntry app_entries[] = 
{
  { "preferences", preferences_activated, NULL, NULL, NULL },
  { "quit", quit_activated , NULL, NULL, NULL }
};

static void
entropy_app_startup (GApplication *app)
{
  GtkBuilder *builder;
  GMenuModel *app_menu;
  
  const gchar *quit_accels[2] = { "<Ctrl>Q", NULL };

  G_APPLICATION_CLASS (entropy_app_parent_class)->startup (app);

  g_action_map_add_action_entries (G_ACTION_MAP (app),
                                   app_entries,
                                   G_N_ELEMENTS (app_entries),
                                   app);

  gtk_application_set_accels_for_action (GTK_APPLICATION (app),
                                         "app.quit",
                                         quit_accels);

  builder = gtk_builder_new_from_resource ("/org/gnome/Entropy/ui/menu.ui");
  app_menu = G_MENU_MODEL (gtk_builder_get_object (builder, "appmenu"));
  gtk_application_set_app_menu (GTK_APPLICATION (app), app_menu);

  g_object_unref (builder);
}

static void
entropy_app_activate (GApplication *app)
{
  EntropyWindow *win;

  win = entropy_window_new (ENTROPY_APP (app));
  gtk_window_present (GTK_WINDOW (win));
}

static void
entropy_app_class_init (EntropyAppClass *class)
{
  G_APPLICATION_CLASS (class)->startup = entropy_app_startup;
  G_APPLICATION_CLASS (class)->activate = entropy_app_activate;
}

EntropyApp *
entropy_app_new (void)
{
  return g_object_new (ENTROPY_TYPE_APP,
                       "application-id", "org.gnome.Entropy",
                       NULL);
}
