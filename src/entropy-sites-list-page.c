/* entropy-list-page.c
 *
 * Copyright 2018 Lennart Hannink <lennart@hannink.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "entropy-sites-list-page.h"

struct _EntropyListPage
{
  GtkBox parent_instance;

  GtkWidget *tree_view;

};

G_DEFINE_TYPE (EntropyListPage, entropy_list_page, GTK_TYPE_BOX)

enum
{
  SITE_CHANGED,
  LAST_SIGNAL
};

enum
{
  COL_SITENAME = 0,
  COL_SITEID,
  NUM_COLS
};

static guint signals [LAST_SIGNAL];

static GtkTreeModel *
create_model_from_user_sites (EntropyMpwOperation *op)
{
  GtkListStore *store;
  GtkTreeIter iter;

  store = gtk_list_store_new (NUM_COLS,
                              G_TYPE_STRING,
                              G_TYPE_INT);

  const int n_sites = entropy_mpw_get_user_sites_count (op);

  if (n_sites <= 0)
    return NULL;
  
  for (int i = 0; i < n_sites; ++i)
  {
    const gchar *sitename = entropy_mpw_get_user_site_name (op, i);
    
    gtk_list_store_append (store, &iter);
    gtk_list_store_set (store, &iter,
                        COL_SITENAME, sitename,
                        COL_SITEID, i,
                        -1);  
  } 
  
  return GTK_TREE_MODEL (store);
}
  
static void
init_tree_view (GtkTreeView *tree)
{
  GtkCellRenderer *renderer;
  renderer = gtk_cell_renderer_text_new();

  g_object_set (renderer, "xalign", 0.5, NULL);
  g_object_set (renderer, "height", 50, NULL);
  /* g_object_set (renderer, "family", "Monospace", NULL); */

  gtk_tree_view_insert_column_with_attributes (tree,
                                               COL_SITENAME,
                                               "Sitename",
                                               renderer,
                                               "text",
                                               COL_SITENAME,
                                               NULL);
}

void 
populate_tree_view_from_user_sites (EntropyListPage     *self,
                                    const EntropyMpwOperation *op)
{
  GtkTreeModel *model = create_model_from_user_sites (op); 

  gtk_tree_view_set_model (GTK_TREE_VIEW (self->tree_view), model);

  g_object_unref (model);
}

static void
on_selection_changed (GtkTreeSelection *selection,
                      gpointer          user_data)
{
  EntropyListPage *page = ENTROPY_LIST_PAGE (user_data);


  GtkTreeModel *model;
  GtkTreeIter iter;
  guint site_id_selected;
  gtk_tree_view_get_model (GTK_TREE_VIEW (page->tree_view));

  if (gtk_tree_selection_get_selected (selection, &model, &iter))
  {
    gtk_tree_model_get (model, &iter, COL_SITEID, &site_id_selected, -1); 
    
    g_signal_emit (page, signals[SITE_CHANGED], 0, site_id_selected);
  }
}

static void
entropy_list_page_init (EntropyListPage *self)
{
  gtk_widget_set_has_window (GTK_WIDGET (self), FALSE);
  gtk_widget_init_template (GTK_WIDGET (self));

  init_tree_view (GTK_TREE_VIEW (self->tree_view));

  /* make sure we are in browse selection mode 
   * (one item is selection at any time)
   */
  GtkTreeSelection *selection;
  selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (self->tree_view));
  gtk_tree_selection_set_mode (selection, GTK_SELECTION_BROWSE);

  g_signal_connect (selection, "changed", G_CALLBACK (on_selection_changed), self);
}

static void
entropy_list_page_class_init (EntropyListPageClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Entropy/ui/entropy-sites-list-page.ui");
  gtk_widget_class_bind_template_child (widget_class, EntropyListPage, tree_view);

  signals[SITE_CHANGED] = 
    g_signal_new ("site-changed",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL, NULL,
                  G_TYPE_NONE,
                  1,
                  G_TYPE_INT);
}
