/* entropy-login-page.h
 *
 * Copyright 2018 Lennart Hannink <lennart@hannink.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>

#include "entropy-mpw.h"

G_BEGIN_DECLS

#define ENTROPY_TYPE_LOGIN_PAGE (entropy_login_page_get_type())

G_DECLARE_FINAL_TYPE (EntropyLoginPage, entropy_login_page, ENTROPY, LOGIN_PAGE, GtkBox)

GtkWidget     *entropy_login_page_new          (void);
void           entropy_login_page_emit_sign_in (EntropyLoginPage    *self,
                                                EntropyMpwOperation *op);

G_END_DECLS

