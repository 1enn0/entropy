/* entropy-sites-page.c
 *
 * Copyright 2018 Lennart Hannink <lennart@hannink.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "entropy-sites-page.h"
#include "entropy-sites-list-page.h"
#include "entropy-sites-edit-page.h"

struct _EntropySitesPage
{
  GtkBox               parent_instance;

  GtkWidget           *stack_sites;
  GtkWidget           *list_page;
  GtkWidget           *edit_page;

  GtkWidget           *button_visible;
  GtkWidget           *label_pw;
  GtkWidget           *button_copy_clipboard;

  EntropyMpwOperation *operation;
  guint                site_id_selected;
};

G_DEFINE_TYPE (EntropySitesPage, entropy_sites_page, GTK_TYPE_BOX)

static void
update_status_bar (EntropySitesPage *page)
{
  const gchar *password;
  if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (page->button_visible)))
  {
    /* now we actually have to compute the site password */
    password = entropy_mpw_get_site_pw (page->operation, page->site_id_selected);

    /* remove checked state flag, so that the toggle button
    / * visually behaves like a normal button*/ 
    gtk_widget_unset_state_flags (GTK_WIDGET (page->button_visible), GTK_STATE_FLAG_CHECKED);
  }
  else
  {
    /* visibility is off, a placeholder will do */
    password = entropy_mpw_get_site_pw_placeholder (page->operation, page->site_id_selected);
  }
  gtk_label_set_text (GTK_LABEL (page->label_pw), password);
}

static void
on_button_visible_toggled (GtkToggleButton *btn_visible,
                           gpointer         user_data)
{
  EntropySitesPage *page = ENTROPY_SITES_PAGE (user_data);
  update_status_bar (page);
}

static void
on_button_copy_clipboard_clicked (GtkButton *button,
                                  gpointer   user_data)
{
  EntropySitesPage *page = ENTROPY_SITES_PAGE (user_data);

  GtkClipboard *clipboard = gtk_clipboard_get (GDK_SELECTION_CLIPBOARD);
  const gchar *password;

  if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (page->button_visible)))
  {
    /* password is visible in label_password */
    password = gtk_label_get_text (GTK_LABEL (page->label_pw));
  }
  else
  {
    /* we have to calculate password */
    password = entropy_mpw_get_site_pw (page->operation, page->site_id_selected);
  }
  gtk_clipboard_set_text (clipboard, password, -1);

  /* zero out password */
  memset ((void*)password, 0, strlen (password));
}

/* static void */
/* label_erase_memory (GtkLabel *label) */
/* { */
/*   const gchar *l = gtk_label_get_label (label); */
/*   const gchar *t = gtk_label_get_text (label); */

/*   memset ((void *)l, 0, strlen(l)); */
/*   memset ((void *)t, 0, strlen(t)); */
/* } */

void
entropy_sites_page_edit_selected (EntropySitesPage *self)
{
  entropy_edit_page_edit (ENTROPY_EDIT_PAGE (self->edit_page),
                          self->operation,
                          self->site_id_selected);
  gtk_stack_set_visible_child_name (GTK_STACK (self->stack_sites), "edit_page");

}

void
entropy_sites_page_finish_edit (EntropySitesPage *self)
{
  gtk_stack_set_visible_child_name (GTK_STACK (self->stack_sites), "list_page");

}

void
entropy_sites_page_set_mpw_operation (EntropySitesPage    *self,
                                      EntropyMpwOperation *op)
{
  if (self->operation)
  {
    g_object_unref (self->operation);
  }
  self->operation = op;
  g_object_ref (self->operation);

  populate_tree_view_from_user_sites (ENTROPY_LIST_PAGE (self->list_page), op);
}

const gchar *
entropy_sites_page_get_site_name_selected (EntropySitesPage *self)
{
  const gchar *sitename;

  sitename = entropy_mpw_get_user_site_name (self->operation,
                                             self->site_id_selected);

  return g_strdup (sitename);
}

static void
on_site_changed (EntropyListPage *list_page,
                 guint            site_id_selected,
                 gpointer         user_data)
{
  EntropySitesPage *page = ENTROPY_SITES_PAGE (user_data);

  page->site_id_selected = site_id_selected;

  update_status_bar (page);
}

static void
recompute_password (EntropyEditPage *edit_page,
                    gpointer         user_data)
{
  EntropySitesPage *page = ENTROPY_SITES_PAGE (user_data);

  entropy_mpw_set_site_result_type (page->operation,
                                    page->site_id_selected,
                                    entropy_edit_page_get_result_type (ENTROPY_EDIT_PAGE (page->edit_page)));
  entropy_mpw_set_site_algo_version (page->operation,
                                    page->site_id_selected,
                                    entropy_edit_page_get_algo_version (ENTROPY_EDIT_PAGE (page->edit_page)));
  entropy_mpw_set_site_counter (page->operation,
                                    page->site_id_selected,
                                    entropy_edit_page_get_counter (ENTROPY_EDIT_PAGE (page->edit_page)));
  
  update_status_bar (page);
}

static void
entropy_sites_page_class_init (EntropySitesPageClass *klass)
{
  g_type_ensure (ENTROPY_TYPE_LIST_PAGE);
  g_type_ensure (ENTROPY_TYPE_EDIT_PAGE);

  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Entropy/ui/entropy-sites-page.ui");

  gtk_widget_class_bind_template_child (widget_class, EntropySitesPage, stack_sites); 
  gtk_widget_class_bind_template_child (widget_class, EntropySitesPage, list_page); 
  gtk_widget_class_bind_template_child (widget_class, EntropySitesPage, edit_page); 
  gtk_widget_class_bind_template_child (widget_class, EntropySitesPage, button_visible); 
  gtk_widget_class_bind_template_child (widget_class, EntropySitesPage, label_pw); 
  gtk_widget_class_bind_template_child (widget_class, EntropySitesPage, button_copy_clipboard); 

}

static void
entropy_sites_page_init (EntropySitesPage *self)
{
  gtk_widget_set_has_window (GTK_WIDGET (self), FALSE);
  gtk_widget_init_template (GTK_WIDGET (self));

  g_signal_connect (self->button_copy_clipboard,
                    "clicked",
                    G_CALLBACK (on_button_copy_clipboard_clicked),
                    self);
  g_signal_connect (self->button_visible,
                    "toggled",
                    G_CALLBACK (on_button_visible_toggled),
                    self);
  g_signal_connect (self->list_page,
                    "site-changed",
                    G_CALLBACK (on_site_changed),
                    self);
  g_signal_connect (self->edit_page,
                    "site-config-changed",
                    G_CALLBACK (recompute_password),
                    self);

  /* Set custom style class for statusbar buttons */
  GtkStyleContext *ctx;
  ctx = gtk_widget_get_style_context (GTK_WIDGET (self->button_visible));
  gtk_style_context_add_class (ctx, "statusbar-button");
  ctx = gtk_widget_get_style_context (GTK_WIDGET (self->button_copy_clipboard));
  gtk_style_context_add_class (ctx, "statusbar-button");

  gtk_label_set_selectable (GTK_LABEL (self->label_pw), 1);
}


GtkWidget *
entropy_sites_page_new (void)
{
  EntropySitesPage *page;

  page = g_object_new (ENTROPY_TYPE_SITES_PAGE, NULL);

  return GTK_WIDGET (page);
}
