/* entropy-edit-page.h
 *
 * Copyright 2018 Lennart Hannink <lennart@hannink.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>

#include "entropy-mpw.h"

G_BEGIN_DECLS

#define ENTROPY_TYPE_EDIT_PAGE (entropy_edit_page_get_type())

G_DECLARE_FINAL_TYPE (EntropyEditPage, entropy_edit_page, ENTROPY, EDIT_PAGE, GtkBox)

void entropy_edit_page_edit (EntropyEditPage     *self,
                             EntropyMpwOperation *op,
                             guint                site_id);

MPResultType       entropy_edit_page_get_result_type  (EntropyEditPage *self);
MPAlgorithmVersion entropy_edit_page_get_algo_version (EntropyEditPage *self);
guint              entropy_edit_page_get_counter      (EntropyEditPage *self);


G_END_DECLS

