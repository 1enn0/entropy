/* entropy-list-page.h
 *
 * Copyright 2018 Lennart Hannink <lennart@hannink.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>

#include "entropy-mpw.h"

G_BEGIN_DECLS

#define ENTROPY_TYPE_LIST_PAGE (entropy_list_page_get_type())

G_DECLARE_FINAL_TYPE (EntropyListPage, entropy_list_page, ENTROPY, LIST_PAGE, GtkBox)

void populate_tree_view_from_user_sites (EntropyListPage     *self,
                                         const EntropyMpwOperation *op);

G_END_DECLS


