/* entropy-window.c
 *
 * Copyright 2018 Lennart Hannink <lennart@hannink.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "entropy-config.h"
#include "entropy-window.h"
#include "entropy-login-page.h"
#include "entropy-sites-page.h"

struct _EntropyWindow
{
  GtkApplicationWindow  parent_instance;

  const gchar         *username;

  /* Template widgets */
  GtkHeaderBar        *header_bar;
  GtkStack            *stack_main;
  GtkStack            *stack_header;

  GtkWidget           *login_page;
  GtkWidget           *sites_page;
  GtkButton           *button_edit;
  GtkButton           *button_back;
};

G_DEFINE_TYPE (EntropyWindow, entropy_window, GTK_TYPE_APPLICATION_WINDOW)

enum
{
  PROP_SUBTITLE = 1,
  N_PROPERTIES
};

static GParamSpec* obj_properties [N_PROPERTIES] = {NULL, };

static void
entropy_window_set_property (GObject      *object,
                             guint         property_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  EntropyWindow *window = ENTROPY_WINDOW (object);

  switch (property_id)
  {
    case PROP_SUBTITLE:
      gtk_header_bar_set_subtitle (window->header_bar, g_value_dup_string (value));
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
entropy_window_get_property (GObject    *object,
                             guint       property_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  EntropyWindow *window = ENTROPY_WINDOW (object);

  const gchar* subtitle;

  switch (property_id)
  {
    case PROP_SUBTITLE:
      subtitle = gtk_header_bar_get_subtitle (window->header_bar);
      g_value_set_string (value, subtitle);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }

}

/* static const gchar * */
/* concat_strings (const gchar *s1, */
/*                 const gchar *s2) */
/* { */
/*   const size_t len_s1 = strlen (s1); */
/*   const size_t len_s2 = strlen (s2); */

/*   gchar* res = malloc (len_s1+len_s2+2); */
/*   sprintf(res, "%s %s", s1, s2); */

/*   const gchar *ret = g_strdup (res); */
/*   free (res); */

/*   return ret; */ 
/* } */

static void
on_button_edit_clicked (GtkButton *button_edit,
                        gpointer   user_data)
{
  EntropyWindow *window = ENTROPY_WINDOW (user_data);
  
  entropy_sites_page_edit_selected (ENTROPY_SITES_PAGE (window->sites_page));
  gtk_stack_set_visible_child_name (window->stack_header, "edit_actions");

  /* const gchar *sitename = entropy_mpw_get_user_site_name (window->op, window->site_id_selected); */
  /* const gchar *subtitle = concat_strings ("Editing", sitename); */
  /* gtk_header_bar_set_subtitle (window->header_bar, subtitle); */
  /* g_free ((gpointer)subtitle); */
}

static void
on_button_back_clicked (GtkButton *button_edit,
                        gpointer   user_data)
{
  EntropyWindow *window = ENTROPY_WINDOW (user_data);
  gtk_stack_set_visible_child_name (window->stack_header, "site_actions");
  entropy_sites_page_finish_edit (ENTROPY_SITES_PAGE (window->sites_page));

  gtk_header_bar_set_subtitle (window->header_bar, window->username);
}

static void
entropy_window_dispose (GObject *gobject)
{
  /* EntropyWindow *window = ENTROPY_WINDOW (gobject); */
  /* if (window && window->op) */
  /* { */
  /*   g_clear_object (&window->op); */
  /* } */

  /* Always chain up to the parent class; there is no need to check if
   * the parent class implements the dispose() virtual function: it is
   * always guaranteed to do so
   */
  G_OBJECT_CLASS (entropy_window_parent_class)->dispose (gobject);
}

static void
entropy_window_finalize (GObject *gobject)
{
  /* Always chain up to the parent class; as with dispose(), finalize()
   * is guaranteed to exist on the parent's class virtual function table
   */
  G_OBJECT_CLASS (entropy_window_parent_class)->finalize (gobject);
}

static void
on_sign_in (EntropyLoginPage    *page,
            EntropyMpwOperation *op,
            gpointer             user_data)
{
  EntropyWindow *window = ENTROPY_WINDOW (user_data);
  entropy_sites_page_set_mpw_operation (ENTROPY_SITES_PAGE (window->sites_page), op);

  gchar *subtitle;
  g_object_get (G_OBJECT (op), "username", &subtitle, NULL); 
  window->username = g_strdup (subtitle);
  gtk_header_bar_set_subtitle (window->header_bar, subtitle);
  g_free ((gpointer) subtitle);

  gtk_stack_set_visible_child_name (window->stack_main, "sites");
  gtk_stack_set_visible_child_name (window->stack_header, "site_actions");
}

static void
entropy_window_class_init (EntropyWindowClass *klass)
{
  /* make sure that our custom types have
   * been registered with the type system
   * so that they can be found by GtkBuilder
   */
  g_type_ensure (ENTROPY_TYPE_LOGIN_PAGE);
  g_type_ensure (ENTROPY_TYPE_SITES_PAGE);

  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Entropy/ui/entropy-window.ui");
  gtk_widget_class_bind_template_child (widget_class, EntropyWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, EntropyWindow, stack_main);
  gtk_widget_class_bind_template_child (widget_class, EntropyWindow, stack_header);

  gtk_widget_class_bind_template_child (widget_class, EntropyWindow, login_page);
  gtk_widget_class_bind_template_child (widget_class, EntropyWindow, sites_page);
  gtk_widget_class_bind_template_child (widget_class, EntropyWindow, button_edit);
  gtk_widget_class_bind_template_child (widget_class, EntropyWindow, button_back);

  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->set_property = entropy_window_set_property;
  object_class->get_property = entropy_window_get_property;
  object_class->dispose = entropy_window_dispose;
  object_class->finalize = entropy_window_finalize;

  obj_properties[PROP_SUBTITLE] = 
    g_param_spec_string ("subtitle",
                         "Subtitle",
                         "Subtitle appearing in the header bar of the applicatiion.",
                         NULL, /* default value */ 
                         G_PARAM_READWRITE);
  g_object_class_install_properties (object_class,
                                     N_PROPERTIES,
                                     obj_properties);

}

static void
entropy_window_init (EntropyWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
  gtk_header_bar_set_title (self->header_bar, "Entropy");

  g_signal_connect (self->button_edit, "clicked", G_CALLBACK (on_button_edit_clicked), self);
  g_signal_connect (self->button_back, "clicked", G_CALLBACK (on_button_back_clicked), self);

  g_signal_connect (self->login_page, "sign-in", G_CALLBACK (on_sign_in), self);

  /* Custom style options */
  GdkScreen *screen = gdk_screen_get_default();
  GtkCssProvider *provider = gtk_css_provider_new();
  gtk_css_provider_load_from_resource (provider, "/org/gnome/Entropy/style/style.css");
  gtk_style_context_add_provider_for_screen (screen, GTK_STYLE_PROVIDER (provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
}

EntropyWindow*
entropy_window_new (EntropyApp *app)
{
  return g_object_new (ENTROPY_TYPE_WINDOW, "application", app, NULL);
}
