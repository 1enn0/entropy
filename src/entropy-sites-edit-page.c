/* entropy-edit-page.c
 *
 * Copyright 2018 Lennart Hannink <lennart@hannink.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "entropy-sites-edit-page.h"
#include "entropy-window.h"

#include "libmpw/mpw-types.h"
#include "libmpw/mpw-algorithm.h"

struct _EntropyEditPage
{
  GtkBox         parent_instance;

  GtkWidget     *combobox_pw_type;
  GtkWidget     *combobox_algo_version;
  GtkWidget     *spinbutton_counter;
};

G_DEFINE_TYPE (EntropyEditPage, entropy_edit_page, GTK_TYPE_BOX)

enum 
{
  COL_RES_TYPE = 0,
  COL_RES_TYPE_NAME,
  NUM_COLS_PW_TYPE
};

static const MPResultType result_types[8] = {
  MPResultTypeTemplateMaximum,
  MPResultTypeTemplateLong,
  MPResultTypeTemplateMedium,
  MPResultTypeTemplateBasic,
  MPResultTypeTemplateShort,
  MPResultTypeTemplatePIN,
  MPResultTypeTemplateName,
  MPResultTypeTemplatePhrase
};

static const gchar* result_types_string[8] = {
  "Maximum",
  "Long",
  "Medium",
  "Basic",
  "Short",
  "PIN",
  "Name",
  "Phrase"
};

enum
{
  COL_ALGO_VERSION = 0,
  COL_ALGO_VERSION_STRING,
  NUM_COLS_ALGO_VERSION
};

static const MPAlgorithmVersion algo_version[4] = {
  MPAlgorithmVersion0,
  MPAlgorithmVersion1,
  MPAlgorithmVersion2,
  MPAlgorithmVersion3
};

static const gchar* algo_version_string[4] = {
  "V0",
  "V1",
  "V2",
  "V3"
};

/* enum */
/* { */
/*   H_PWTYPE, */
/*   H_ALGO, */
/*   H_COUNTER, */
/*   NUM_HANDLERS */
/* }; */

enum
{
  SITE_CONFIG_CHANGED,
  LAST_SIGNAL
};

/* static gulong handlers [NUM_HANDLERS]; */
static guint signals [LAST_SIGNAL];

static GtkTreeModel *
create_and_populate_model_pw_type ()
{
  GtkListStore *store;
  GtkTreeIter iter;

  store = gtk_list_store_new (NUM_COLS_PW_TYPE,
                              G_TYPE_INT,
                              G_TYPE_STRING);

  int n_res_types = sizeof(result_types) / sizeof(result_types[0]);
  for (int i = 0; i < n_res_types; ++i)
  {
    gtk_list_store_append (store, &iter);  
    gtk_list_store_set (store, &iter,
                        COL_RES_TYPE, result_types[i],
                        COL_RES_TYPE_NAME, result_types_string[i],
                        -1);
  }
  return GTK_TREE_MODEL (store);
}

static void
populate_combo_pw_type (GtkComboBox *combo_box)
{
  GtkTreeModel *model = create_and_populate_model_pw_type();
  gtk_combo_box_set_model (combo_box, model);

  GtkCellRenderer *renderer;
  renderer = gtk_cell_renderer_text_new ();
  gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo_box), renderer, TRUE);
  gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (combo_box), 
                                 renderer,
                                 "text",
                                 COL_RES_TYPE_NAME,
                                 NULL);
  gtk_combo_box_set_active (combo_box, 1);

  g_object_unref (model);
}

static GtkTreeModel *
create_and_populate_model_algo_version ()
{
  GtkListStore *store;
  GtkTreeIter iter;

  store = gtk_list_store_new (NUM_COLS_ALGO_VERSION,
                              G_TYPE_INT,
                              G_TYPE_STRING);

  int n_rows = sizeof(algo_version) / sizeof(algo_version[0]);
  for (int i = 0; i < n_rows; ++i)
  {
    gtk_list_store_append (store, &iter);  
    gtk_list_store_set (store, &iter,
                        COL_ALGO_VERSION, algo_version[i],
                        COL_ALGO_VERSION_STRING, algo_version_string[i],
                        -1);
  }
  return GTK_TREE_MODEL (store);
}

static void
populate_combo_algo_version (GtkComboBox *combo_box)
{
  GtkTreeModel *model = create_and_populate_model_algo_version();
  gtk_combo_box_set_model (combo_box, model);

  GtkCellRenderer *renderer;
  renderer = gtk_cell_renderer_text_new ();
  gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo_box), renderer, TRUE);
  gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (combo_box),
                                  renderer,
                                  "text",
                                  COL_ALGO_VERSION_STRING,
                                  NULL);
  gtk_combo_box_set_active (combo_box, 3);

  g_object_unref (model);
}

/* static void */
/* on_counter_value_changed (GtkSpinButton *spin_button, */
/*                           gpointer       user_data) */
/* { */
/*   EntropyMpwOperation *op = ENTROPY_MPWOPERATION (user_data); */
/*   gdouble val = gtk_spin_button_get_value (spin_button); */  

   

/* } */

MPResultType
entropy_edit_page_get_result_type (EntropyEditPage *self)
{
  GtkTreeModel *model;
  GtkTreeIter iter;
  MPResultType result_type;
  model =  gtk_combo_box_get_model (GTK_COMBO_BOX (self->combobox_pw_type));
  gtk_combo_box_get_active_iter (GTK_COMBO_BOX (self->combobox_pw_type), &iter);
  gtk_tree_model_get (model, &iter, COL_RES_TYPE, &result_type, -1);

  return result_type;
}

MPAlgorithmVersion 
entropy_edit_page_get_algo_version (EntropyEditPage *self)
{
  GtkTreeModel *model;
  GtkTreeIter iter;
  MPAlgorithmVersion algo_version;
  model =  gtk_combo_box_get_model (GTK_COMBO_BOX (self->combobox_algo_version));
  gtk_combo_box_get_active_iter (GTK_COMBO_BOX (self->combobox_algo_version), &iter);
  gtk_tree_model_get (model, &iter, COL_ALGO_VERSION, &algo_version, -1);

  return algo_version;

}

guint
entropy_edit_page_get_counter (EntropyEditPage *self)
{
  gdouble value = gtk_spin_button_get_value (GTK_SPIN_BUTTON (self->spinbutton_counter));

  return (guint) value;
}

void
entropy_edit_page_edit (EntropyEditPage     *self,
                        EntropyMpwOperation *op,
                        guint                site_id)
{
  MPMarshalledSite *site = entropy_mpw_get_site (op, site_id);
  
  GtkWidget *toplevel = gtk_widget_get_toplevel (GTK_WIDGET (self));
  if (gtk_widget_is_toplevel (toplevel))
  {
    EntropyWindow *window = ENTROPY_WINDOW (toplevel);
    g_object_set (G_OBJECT (window), "subtitle", site->name, NULL);
  }

  gint idx;
  switch (site->type)
  {
    case MPResultTypeTemplateMaximum:
      idx = 0;
      break;
    case MPResultTypeTemplateLong:
      idx = 1;
      break;
    case MPResultTypeTemplateMedium:
      idx = 2;
      break;
    case MPResultTypeTemplateBasic:
      idx = 3;
      break;
    case MPResultTypeTemplateShort:
      idx = 4;
      break;
    case MPResultTypeTemplatePIN:
      idx = 5;
      break;
    case MPResultTypeTemplateName:
      idx = 6;
      break;
    case MPResultTypeTemplatePhrase:
      idx = 7;
      break;
  }
  gtk_combo_box_set_active (GTK_COMBO_BOX (self->combobox_pw_type), idx);

  
  switch (site->algorithm)
  {
    case MPAlgorithmVersion0:
      idx = 0;
      break;
    case MPAlgorithmVersion1:
      idx = 1;
      break;
    case MPAlgorithmVersion2:
      idx = 2;
      break;
    case MPAlgorithmVersion3:
      idx = 3;
      break;
  }
  gtk_combo_box_set_active (GTK_COMBO_BOX (self->combobox_algo_version), idx);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (self->spinbutton_counter),
                             site->counter);
};

/* static void */
/* entropy_edit_page_end_edit (EntropyEditPage *page) */
/* { */
/* } */

static void
entropy_edit_page_class_init (EntropyEditPageClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Entropy/ui/entropy-sites-edit-page.ui");
  gtk_widget_class_bind_template_child (widget_class, EntropyEditPage, combobox_pw_type);
  gtk_widget_class_bind_template_child (widget_class, EntropyEditPage, combobox_algo_version);
  gtk_widget_class_bind_template_child (widget_class, EntropyEditPage, spinbutton_counter);

  signals[SITE_CONFIG_CHANGED] = 
   g_signal_new ("site-config-changed",
                 G_TYPE_FROM_CLASS (klass),
                 G_SIGNAL_RUN_LAST,
                 0,
                 NULL, NULL, NULL,
                 G_TYPE_NONE, 0);
}

static void
entropy_edit_page_emit_changed (gpointer instance,
                                gpointer user_data)
{
  EntropyEditPage *page = ENTROPY_EDIT_PAGE (user_data);
  g_signal_emit (page, signals[SITE_CONFIG_CHANGED], 0, NULL);
}

static void
entropy_edit_page_init (EntropyEditPage *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
  gtk_widget_set_has_window (GTK_WIDGET (self), FALSE);

  populate_combo_pw_type (GTK_COMBO_BOX (self->combobox_pw_type));
  populate_combo_algo_version (GTK_COMBO_BOX (self->combobox_algo_version));

  GtkAdjustment *adjustment;
  adjustment = gtk_adjustment_new (1.0, 1.0, 10000.0, 1.0, 1.0, 0.0);
  gtk_spin_button_set_adjustment (GTK_SPIN_BUTTON (self->spinbutton_counter), adjustment);

  g_signal_connect (self->combobox_pw_type, "changed", G_CALLBACK (entropy_edit_page_emit_changed), self);
  g_signal_connect (self->combobox_algo_version, "changed", G_CALLBACK (entropy_edit_page_emit_changed), self);
  g_signal_connect (self->spinbutton_counter, "value-changed", G_CALLBACK (entropy_edit_page_emit_changed), self);
}
