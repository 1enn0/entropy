/* entropy-login-page.c
 *
 * Copyright 2018 Lennart Hannink <lennart@hannink.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "entropy-login-page.h"

struct _EntropyLoginPage
{
  GtkBox     parent_instance;

  GtkWidget *entry_user;
  GtkWidget *entry_mpw;
  GtkWidget *label_identicon;
  GtkWidget *button_sign_in;
};

G_DEFINE_TYPE (EntropyLoginPage, entropy_login_page, GTK_TYPE_BOX)

enum
{
  SIGN_IN,
  LAST_SIGNAL
};

static guint signals [LAST_SIGNAL];

void
entropy_login_page_emit_sign_in (EntropyLoginPage    *self,
                                 EntropyMpwOperation *op)
{
  g_signal_emit (self, signals[SIGN_IN], 0, op);
}

static void
update_identicon (GtkEditable *editable,
                  gpointer     user_data)
{
  EntropyLoginPage *page = ENTROPY_LOGIN_PAGE (user_data);

  const gchar *user = gtk_entry_get_text (GTK_ENTRY (page->entry_user));
  const gchar *mpw = gtk_entry_get_text (GTK_ENTRY (page->entry_mpw));

  const gchar *identicon = entropy_mpw_process_identicon (user, mpw);

  /* in case either of the two entries don't have
   * any text, <identicon> will be NULL. this is handled
   * by the GtkLabel internally (label is cleared),
   * so we need not worry about it here
   */
  gtk_label_set_text (GTK_LABEL (page->label_identicon), identicon);
}

static void
on_button_sign_in_clicked (gpointer instance,
                           gpointer user_data)
{
  EntropyLoginPage *page = ENTROPY_LOGIN_PAGE (user_data);

  const gchar *user = gtk_entry_get_text (GTK_ENTRY (page->entry_user));
  const gchar *mpw = gtk_entry_get_text (GTK_ENTRY (page->entry_mpw));

  if (!user || !mpw || (user[0] == '\0') || (mpw[0] == '\0'))
    return;

  EntropyMpwOperation *op = ENTROPY_MPWOPERATION (entropy_mpw_new());
  g_object_set (G_OBJECT (op),
                "username", user,
                "mpw", mpw,
                NULL);
  /* entropy_mpw_set_full_name (op, user); */
  /* entropy_mpw_set_master_pw (op, mpw); */

  entropy_mpw_sign_in (op); 

  entropy_login_page_emit_sign_in (page, op);

  /* EntropySitesPage will keep its own reference
   * to the EntropyMpwOperation so we can unref it here */
  g_object_unref (op);

  // populate GtkTreeView in EntropyListPage
  // switch visitble child of stack_main to list_page
  // handle login errors...
}

static void
entropy_login_page_init (EntropyLoginPage *page)
{
  gtk_widget_set_has_window (GTK_WIDGET (page), FALSE);
  gtk_widget_init_template (GTK_WIDGET (page));

  /* connect the signals */
  g_signal_connect (page->entry_user, "changed", G_CALLBACK (update_identicon), page);
  g_signal_connect (page->entry_user, "activate", G_CALLBACK (on_button_sign_in_clicked), page);
  g_signal_connect (page->entry_mpw, "changed", G_CALLBACK (update_identicon), page);
  g_signal_connect (page->entry_mpw, "activate", G_CALLBACK (on_button_sign_in_clicked), page);
  g_signal_connect (page->button_sign_in, "clicked", G_CALLBACK (on_button_sign_in_clicked), page);
}

static void
entropy_login_page_class_init (EntropyLoginPageClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Entropy/ui/entropy-login-page.ui"); 

  gtk_widget_class_bind_template_child (widget_class, EntropyLoginPage, entry_user);
  gtk_widget_class_bind_template_child (widget_class, EntropyLoginPage, entry_mpw);
  gtk_widget_class_bind_template_child (widget_class, EntropyLoginPage, label_identicon);
  gtk_widget_class_bind_template_child (widget_class, EntropyLoginPage, button_sign_in);

  signals[SIGN_IN] = 
    g_signal_new ("sign-in",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL, NULL,
                  G_TYPE_NONE,
                  1,
                  ENTROPY_TYPE_MPWOPERATION);
}

GtkWidget*
entropy_login_page_new (void)
{
  EntropyLoginPage *page;

  page = g_object_new (ENTROPY_TYPE_LOGIN_PAGE, NULL);

  return GTK_WIDGET (page);
}

