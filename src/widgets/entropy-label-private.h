/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ENTROPY_LABEL_PRIVATE_H__
#define __ENTROPY_LABEL_PRIVATE_H__


#include "widgets/entropy-label.h"


G_BEGIN_DECLS

void _entropy_label_mnemonics_visible_apply_recursively (GtkWidget *widget,
                                                     gboolean   mnemonics_visible);
gint _entropy_label_get_cursor_position (EntropyLabel *label);
gint _entropy_label_get_selection_bound (EntropyLabel *label);

gint         _entropy_label_get_n_links     (EntropyLabel *label);
gint         _entropy_label_get_link_at     (EntropyLabel *label,
                                         gint      pos);
void         _entropy_label_activate_link   (EntropyLabel *label, 
                                         gint      idx);
const gchar *_entropy_label_get_link_uri    (EntropyLabel *label,
                                         gint      idx);
void         _entropy_label_get_link_extent (EntropyLabel *label,
                                         gint      idx,
                                         gint     *start,
                                         gint     *end);
gboolean     _entropy_label_get_link_visited (EntropyLabel *label,
                                          gint      idx);
gboolean     _entropy_label_get_link_focused (EntropyLabel *label,
                                          gint      idx);
                             
G_END_DECLS

#endif /* __ENTROPY_LABEL_PRIVATE_H__ */
