/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Modified by the GTK+ Team and others 1997-2000.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/.
 */

#ifndef __ENTROPY_LABEL_H__
#define __ENTROPY_LABEL_H__

#if !defined (__GTK_H_INSIDE__) && !defined (GTK_COMPILATION)
#error "Only <gtk/gtk.h> can be included directly."
#endif

#include <gtk/deprecated/gtkmisc.h>
#include <gtk/gtkwindow.h>
#include <gtk/gtkmenu.h>

G_BEGIN_DECLS

#define ENTROPY_TYPE_LABEL		  (entropy_label_get_type ())
#define ENTROPY_LABEL(obj)		  (G_TYPE_CHECK_INSTANCE_CAST ((obj), ENTROPY_TYPE_LABEL, EntropyLabel))
#define ENTROPY_LABEL_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass), ENTROPY_TYPE_LABEL, EntropyLabelClass))
#define ENTROPY_IS_LABEL(obj)	  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), ENTROPY_TYPE_LABEL))
#define ENTROPY_IS_LABEL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), ENTROPY_TYPE_LABEL))
#define ENTROPY_LABEL_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), ENTROPY_TYPE_LABEL, EntropyLabelClass))


typedef struct _EntropyLabel              EntropyLabel;
typedef struct _EntropyLabelPrivate       EntropyLabelPrivate;
typedef struct _EntropyLabelClass         EntropyLabelClass;

typedef struct _EntropyLabelSelectionInfo EntropyLabelSelectionInfo;

struct _EntropyLabel
{
  GtkMisc misc;

  /*< private >*/
  EntropyLabelPrivate *priv;
};

struct _EntropyLabelClass
{
  GtkMiscClass parent_class;

  void (* move_cursor)     (EntropyLabel       *label,
			    GtkMovementStep step,
			    gint            count,
			    gboolean        extend_selection);
  void (* copy_clipboard)  (EntropyLabel       *label);

  /* Hook to customize right-click popup for selectable labels */
  void (* populate_popup)   (EntropyLabel       *label,
                             GtkMenu        *menu);

  gboolean (*activate_link) (EntropyLabel       *label,
                             const gchar    *uri);

  /* Padding for future expansion */
  void (*_gtk_reserved1) (void);
  void (*_gtk_reserved2) (void);
  void (*_gtk_reserved3) (void);
  void (*_gtk_reserved4) (void);
  void (*_gtk_reserved5) (void);
  void (*_gtk_reserved6) (void);
  void (*_gtk_reserved7) (void);
  void (*_gtk_reserved8) (void);
};

GDK_AVAILABLE_IN_ALL
GType                 entropy_label_get_type          (void) G_GNUC_CONST;
GDK_AVAILABLE_IN_ALL
GtkWidget*            entropy_label_new               (const gchar   *str);
GDK_AVAILABLE_IN_ALL
GtkWidget*            entropy_label_new_with_mnemonic (const gchar   *str);
GDK_AVAILABLE_IN_ALL
void                  entropy_label_set_text          (EntropyLabel      *label,
						   const gchar   *str);
GDK_AVAILABLE_IN_ALL
const gchar*          entropy_label_get_text          (EntropyLabel      *label);
GDK_AVAILABLE_IN_ALL
void                  entropy_label_set_attributes    (EntropyLabel      *label,
						   PangoAttrList *attrs);
GDK_AVAILABLE_IN_ALL
PangoAttrList        *entropy_label_get_attributes    (EntropyLabel      *label);
GDK_AVAILABLE_IN_ALL
void                  entropy_label_set_label         (EntropyLabel      *label,
						   const gchar   *str);
GDK_AVAILABLE_IN_ALL
const gchar *         entropy_label_get_label         (EntropyLabel      *label);
GDK_AVAILABLE_IN_ALL
void                  entropy_label_set_markup        (EntropyLabel      *label,
						   const gchar   *str);
GDK_AVAILABLE_IN_ALL
void                  entropy_label_set_use_markup    (EntropyLabel      *label,
						   gboolean       setting);
GDK_AVAILABLE_IN_ALL
gboolean              entropy_label_get_use_markup    (EntropyLabel      *label);
GDK_AVAILABLE_IN_ALL
void                  entropy_label_set_use_underline (EntropyLabel      *label,
						   gboolean       setting);
GDK_AVAILABLE_IN_ALL
gboolean              entropy_label_get_use_underline (EntropyLabel      *label);

GDK_AVAILABLE_IN_ALL
void     entropy_label_set_markup_with_mnemonic       (EntropyLabel         *label,
						   const gchar      *str);
GDK_AVAILABLE_IN_ALL
guint    entropy_label_get_mnemonic_keyval            (EntropyLabel         *label);
GDK_AVAILABLE_IN_ALL
void     entropy_label_set_mnemonic_widget            (EntropyLabel         *label,
						   GtkWidget        *widget);
GDK_AVAILABLE_IN_ALL
GtkWidget *entropy_label_get_mnemonic_widget          (EntropyLabel         *label);
GDK_AVAILABLE_IN_ALL
void     entropy_label_set_text_with_mnemonic         (EntropyLabel         *label,
						   const gchar      *str);
GDK_AVAILABLE_IN_ALL
void     entropy_label_set_justify                    (EntropyLabel         *label,
						   GtkJustification  jtype);
GDK_AVAILABLE_IN_ALL
GtkJustification entropy_label_get_justify            (EntropyLabel         *label);
GDK_AVAILABLE_IN_ALL
void     entropy_label_set_ellipsize		  (EntropyLabel         *label,
						   PangoEllipsizeMode mode);
GDK_AVAILABLE_IN_ALL
PangoEllipsizeMode entropy_label_get_ellipsize        (EntropyLabel         *label);
GDK_AVAILABLE_IN_ALL
void     entropy_label_set_width_chars		  (EntropyLabel         *label,
						   gint              n_chars);
GDK_AVAILABLE_IN_ALL
gint     entropy_label_get_width_chars                (EntropyLabel         *label);
GDK_AVAILABLE_IN_ALL
void     entropy_label_set_max_width_chars    	  (EntropyLabel         *label,
					  	   gint              n_chars);
GDK_AVAILABLE_IN_ALL
gint     entropy_label_get_max_width_chars  	  (EntropyLabel         *label);
GDK_AVAILABLE_IN_3_10
void     entropy_label_set_lines                      (EntropyLabel         *label,
                                                   gint              lines);
GDK_AVAILABLE_IN_3_10
gint     entropy_label_get_lines                      (EntropyLabel         *label);
GDK_AVAILABLE_IN_ALL
void     entropy_label_set_pattern                    (EntropyLabel         *label,
						   const gchar      *pattern);
GDK_AVAILABLE_IN_ALL
void     entropy_label_set_line_wrap                  (EntropyLabel         *label,
						   gboolean          wrap);
GDK_AVAILABLE_IN_ALL
gboolean entropy_label_get_line_wrap                  (EntropyLabel         *label);
GDK_AVAILABLE_IN_ALL
void     entropy_label_set_line_wrap_mode             (EntropyLabel         *label,
						   PangoWrapMode     wrap_mode);
GDK_AVAILABLE_IN_ALL
PangoWrapMode entropy_label_get_line_wrap_mode        (EntropyLabel         *label);
GDK_AVAILABLE_IN_ALL
void     entropy_label_set_selectable                 (EntropyLabel         *label,
						   gboolean          setting);
GDK_AVAILABLE_IN_ALL
gboolean entropy_label_get_selectable                 (EntropyLabel         *label);
GDK_AVAILABLE_IN_ALL
void     entropy_label_set_angle                      (EntropyLabel         *label,
						   gdouble           angle);
GDK_AVAILABLE_IN_ALL
gdouble  entropy_label_get_angle                      (EntropyLabel         *label);
GDK_AVAILABLE_IN_ALL
void     entropy_label_select_region                  (EntropyLabel         *label,
						   gint              start_offset,
						   gint              end_offset);
GDK_AVAILABLE_IN_ALL
gboolean entropy_label_get_selection_bounds           (EntropyLabel         *label,
                                                   gint             *start,
                                                   gint             *end);

GDK_AVAILABLE_IN_ALL
PangoLayout *entropy_label_get_layout         (EntropyLabel *label);
GDK_AVAILABLE_IN_ALL
void         entropy_label_get_layout_offsets (EntropyLabel *label,
                                           gint     *x,
                                           gint     *y);

GDK_AVAILABLE_IN_ALL
void         entropy_label_set_single_line_mode  (EntropyLabel *label,
                                              gboolean single_line_mode);
GDK_AVAILABLE_IN_ALL
gboolean     entropy_label_get_single_line_mode  (EntropyLabel *label);

GDK_AVAILABLE_IN_ALL
const gchar *entropy_label_get_current_uri (EntropyLabel *label);
GDK_AVAILABLE_IN_ALL
void         entropy_label_set_track_visited_links  (EntropyLabel *label,
                                                 gboolean  track_links);
GDK_AVAILABLE_IN_ALL
gboolean     entropy_label_get_track_visited_links  (EntropyLabel *label);

GDK_AVAILABLE_IN_3_16
void         entropy_label_set_xalign (EntropyLabel *label,
                                   gfloat    xalign);

GDK_AVAILABLE_IN_3_16
gfloat       entropy_label_get_xalign (EntropyLabel *label);

GDK_AVAILABLE_IN_3_16
void         entropy_label_set_yalign (EntropyLabel *label,
                                   gfloat    yalign);

GDK_AVAILABLE_IN_3_16
gfloat       entropy_label_get_yalign (EntropyLabel *label);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(EntropyLabel, g_object_unref)

G_END_DECLS

#endif /* __ENTROPY_LABEL_H__ */
