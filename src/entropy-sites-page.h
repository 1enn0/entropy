/* entropy-sites-page.h
 *
 * Copyright 2018 Lennart Hannink <lennart@hannink.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>

#include "entropy-mpw.h"

G_BEGIN_DECLS

#define ENTROPY_TYPE_SITES_PAGE (entropy_sites_page_get_type())

G_DECLARE_FINAL_TYPE (EntropySitesPage, entropy_sites_page, ENTROPY, SITES_PAGE, GtkBox)

GtkWidget   *entropy_sites_page_new                    (void);

void         entropy_sites_page_set_mpw_operation      (EntropySitesPage    *self,
                                                        EntropyMpwOperation *op);
void         entropy_sites_page_edit_selected          (EntropySitesPage *self);
void         entropy_sites_page_finish_edit            (EntropySitesPage *self);

const gchar *entropy_sites_page_get_site_name_selected (EntropySitesPage *self);


G_END_DECLS


