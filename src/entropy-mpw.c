/* entropy-mpw.c
 *
 * Copyright 2018 Lennart Hannink
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <unistd.h>
#include <errno.h>

#include "entropy-mpw.h"

struct _EntropyMpwOperation
{
  GObject parent_instance;

  /* bool allowPasswordUpdate; */
  bool sitesFormatFixed;
  const gchar *fullName;
  const gchar *masterPassword;
  MPMasterKey masterKey;
  MPAlgorithmVersion algoVersion;
  /* const gchar *siteName; */
  MPMarshalFormat sitesFormat;
  MPKeyPurpose keyPurpose;
  const gchar *keyContext;
  const gchar *sitesPath;
  MPResultType resultType;
  /* MPCounterValue siteCounter; */
  /* const gchar *purposeResult; */
  /* const gchar *resultState; */
  const gchar *resultParam;
  MPMarshalledUser *user;
  MPMarshalledSite *site;
  /* MPMarshalledQuestion *question; */
};

G_DEFINE_TYPE (EntropyMpwOperation, entropy_mpw_operation, G_TYPE_OBJECT)

enum
{
  PROP_USERNAME = 1,
  PROP_MPW,
  N_PROPERTIES
};

static GParamSpec *obj_properties[N_PROPERTIES] = {NULL, };

static void
entropy_mpw_operation_set_property (GObject      *object,
                                    guint         property_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
  EntropyMpwOperation *self = ENTROPY_MPWOPERATION (object);

  switch (property_id)
  {
    case PROP_USERNAME:
      g_free ((gpointer) self->fullName);
      self->fullName = g_value_dup_string (value);
      break;
    case PROP_MPW:
      g_free ((gpointer) self->masterPassword);
      self->masterPassword = g_value_dup_string (value);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
entropy_mpw_operation_get_property (GObject    *object,
                                    guint       property_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
  EntropyMpwOperation *self = ENTROPY_MPWOPERATION (object);

  switch (property_id)
  {
    case PROP_USERNAME:
      g_value_set_string (value, self->fullName);
      break;
    case PROP_MPW:
      g_value_set_string (value, self->masterPassword);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
entropy_mpw_operation_dispose (GObject *gobject)
{
  EntropyMpwOperation *op = ENTROPY_MPWOPERATION (gobject);
 
  if (op->masterPassword)
    memset ((void*) op->masterPassword, 0, strlen (op->masterPassword));
  if (op->masterKey)
    memset ((void*) op->masterKey, 0, MPMasterKeySize);

  /* Always chain up to the parent class; there is no need to check if
   * the parent class implements the dispose() virtual function: it is
   * always guaranteed to do so
   */
  G_OBJECT_CLASS (entropy_mpw_operation_parent_class)->dispose (gobject);
}

static void
entropy_mpw_operation_finalize (GObject *gobject)
{
  /* Always chain up to the parent class; as with dispose(), finalize()
   * is guaranteed to exist on the parent's class virtual function table
   */
  G_OBJECT_CLASS (entropy_mpw_operation_parent_class)->finalize (gobject);
}

MPMarshalledSite *
entropy_mpw_get_site (EntropyMpwOperation *self,
                      const guint          site_id)
{
  if (!self->user)
    return NULL;

  MPMarshalledUser *user = self->user;
  if (site_id >= user->sites_count)
    return NULL;

  return &user->sites[site_id];
};

static void
entropy_mpw_operation_class_init (EntropyMpwOperationClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->set_property = entropy_mpw_operation_set_property;
  object_class->get_property = entropy_mpw_operation_get_property;
  object_class->dispose = entropy_mpw_operation_dispose;
  object_class->finalize = entropy_mpw_operation_finalize;

  obj_properties[PROP_USERNAME] =
    g_param_spec_string ("username",
                         "Username",
                         "Name of the user currently logged in.",
                         NULL, /* default value */
                         G_PARAM_READWRITE);

  obj_properties[PROP_MPW] =
    g_param_spec_string ("mpw",
                         "MasterPassword",
                         "MasterPassword of the user currently logged in.",
                         NULL, /* default value */
                         G_PARAM_READWRITE);

  g_object_class_install_properties (object_class,
                                     N_PROPERTIES,
                                     obj_properties);
}

static void
entropy_mpw_operation_init (EntropyMpwOperation *self)
{
  /* self->allowPasswordUpdate = false; */
  self->sitesFormatFixed = false;
  self->fullName = NULL;
  self->masterPassword = NULL;
  self->masterKey = NULL;
  self->algoVersion = MPAlgorithmVersionCurrent;
  /* self->siteName = NULL; */
  self->sitesFormat = MPMarshalFormatFlat;
  self->keyPurpose = MPKeyPurposeAuthentication;
  /* self->keyContext = NULL; */
  self->sitesPath = NULL;
  self->resultType = MPResultTypeDefault;
  /* self->siteCounter = MPCounterValueDefault; */
  /* self->purposeResult = NULL; */
  /* self->resultState = NULL; */
  self->resultParam = NULL;
  self->user = NULL;
  self->site = NULL;
  /* self->question = NULL; */
}

const gchar *
entropy_mpw_process_identicon (const gchar *full_name,
                               const gchar *master_password)
{
  if (full_name && master_password)
  {
    MPIdenticon identicon = mpw_identicon (full_name,
                                           master_password);
    // identicon consists of 4 unicode character (4 B each)
    char *res = g_malloc (sizeof(*res) * 4 * 4);

    sprintf (res, "%s%s%s%s",
             identicon.leftArm,
             identicon.body,
             identicon.rightArm,
             identicon.accessory);

    const gchar *ret = g_strdup (res);
    g_free (res);
    return ret;
  }
  return NULL;
}

const gchar *
entropy_mpw_get_full_name (EntropyMpwOperation *self)
{
  return self->fullName;
}

void
entropy_mpw_set_full_name (EntropyMpwOperation *self,
                           const gchar  *full_name)
{
  if (g_strcmp0 (full_name, self->fullName))
  {
    g_free ((gpointer)self->fullName);
    self->fullName = g_strdup (full_name);
  }
}

const gchar *
entropy_mpw_get_master_pw (EntropyMpwOperation *self)
{
  return self->masterPassword;
}

void
entropy_mpw_set_master_pw (EntropyMpwOperation *self,
                                 const gchar  *master_password)
{
  if (g_strcmp0 (master_password, self->masterPassword))
  {
    g_free ((gpointer)self->masterPassword);
    self->masterPassword = g_strdup (master_password);
  }
}

static void
entropy_mpw_generate_masterkey (EntropyMpwOperation *self)
{
  if (self->fullName && self->masterPassword)
  {
    self->masterKey = mpw_masterKey (self->fullName,
                                     self->masterPassword,
                                     self->algoVersion); 
  }
}
 
char*
entropy_mpw_read_file (FILE *file)
{
  if (!file)
    return NULL;

  char *buf = NULL;
  size_t blockSize = 4096, bufSize = 0, bufOffset = 0, readSize = 0;
  while ((mpw_realloc(&buf, &bufSize, blockSize)) &&
         (bufOffset += (readSize = fread(buf + bufOffset, 1, blockSize, file))) &&
         (readSize == blockSize))
    ;

  return buf;
}

const char * 
entropy_mpw_path (const char *prefix,
                  const char *extension)
{
  char *home_dir = NULL;
  if ((home_dir = getenv ("HOME")))
  {
    home_dir = mpw_strdup (home_dir); 
  }
  if (!home_dir)
  {
    home_dir = getcwd (NULL, 0);  
  }

  // Compose filename
  char *path = mpw_strdup (mpw_str ("%s.%s", prefix, extension));

  // Compose pathname
  if (home_dir)
  {
    const char* home_path = mpw_str ("%s/.mpw.d/%s", home_dir, path);
    free (home_dir);
    free (path);
    
    if (home_path)
    {
      path = mpw_strdup (home_path); 
    } 
  }

  return path;
}

static void
entropy_mpw_read_user_sites (EntropyMpwOperation *op)
{
  FILE *sitesFile = NULL;

  mpw_free_string(&op->sitesPath);
  op->sitesPath = entropy_mpw_path(op->fullName, mpw_marshal_format_extension(op->sitesFormat));
  if (!op->sitesPath || !(sitesFile = fopen(op->sitesPath, "r")))
    {
      dbg("Couldn't open configuration file:\n  %s: %s\n", op->sitesPath, strerror(errno));

      // Try to fall back to the flat format.
      if (!op->sitesFormatFixed)
        {
          mpw_free_string(&op->sitesPath);
          op->sitesPath = entropy_mpw_path(op->fullName, mpw_marshal_format_extension(MPMarshalFormatFlat));
          if (op->sitesPath && (sitesFile = fopen(op->sitesPath, "r")))
            op->sitesFormat = MPMarshalFormatFlat;
          else
            dbg("Couldn't open configuration file:\n  %s: %s\n", op->sitesPath, strerror(errno));
        }
    }

  // Load the user object from mpsites.
  if (!sitesFile)
    mpw_free_string(&op->sitesPath);

  else
    {
      // Read file.
      char *sitesInputData = entropy_mpw_read_file(sitesFile);
      if (ferror(sitesFile))
        wrn("Error while reading configuration file:\n  %s: %d\n", op->sitesPath, ferror(sitesFile));
      fclose(sitesFile);

      // Parse file.
      MPMarshalInfo *sitesInputInfo = mpw_marshal_read_info(sitesInputData);

      op->algoVersion = sitesInputInfo->algorithm;

      MPMarshalFormat sitesInputFormat = sitesInputInfo->format;
      MPMarshalError marshalError = { .type = MPMarshalSuccess };
      mpw_marshal_info_free(&sitesInputInfo);
      mpw_marshal_free(&op->user);
      op->user = mpw_marshal_read(sitesInputData, sitesInputFormat, op->masterPassword, &marshalError);
      
      // TODO fix this
      /* if (marshalError.type == MPMarshalErrorMasterPassword && op->allowPasswordUpdate) */
      /*   { */
      /*     // Update master password in mpsites. */
      /*     while (marshalError.type == MPMarshalErrorMasterPassword) */
      /*       { */
      /*         inf("Given master password does not match configuration.\n"); */
      /*         inf("To update the configuration with this new master password, first confirm the old master password.\n"); */

      /*         const char *importMasterPassword = NULL; */
      /*         while (!importMasterPassword || !strlen(importMasterPassword)) */
      /*           importMasterPassword = mpw_getpass("Old master password: "); */

      /*         mpw_marshal_free(&op->user); */
      /*         op->user = mpw_marshal_read(sitesInputData, sitesInputFormat, importMasterPassword, &marshalError); */
      /*         mpw_free_string(&importMasterPassword); */
      /*       } */
      /*     if (op->user) */
      /*       { */
      /*         mpw_free_string(&op->user->masterPassword); */
      /*         op->user->masterPassword = mpw_strdup(op->masterPassword); */
      /*       } */
      /*   } */
      mpw_free_string(&sitesInputData);

      // Incorrect master password.
      if (marshalError.type == MPMarshalErrorMasterPassword)
        {
          ftl("Incorrect master password according to configuration:\n  %s: %s\n", op->sitesPath, marshalError.description);
          // TODO fix this
          //cli_free(args, op);
          //exit(EX_DATAERR);
        }

      // Any other parse error.
      if (!op->user || marshalError.type != MPMarshalSuccess)
        {
          err("Couldn't parse configuration file:\n  %s: %s\n", op->sitesPath, marshalError.description);
          // TODO fix this
          //cli_free(args, op);
          //exit(EX_DATAERR);
        }
    }

  // If no user from mpsites, create a new one.
  if (!op->user)
    op->user = mpw_marshal_user(
      op->fullName, op->masterPassword, MPAlgorithmVersionCurrent);


}
  
void
entropy_mpw_sign_in (EntropyMpwOperation *self)
{
  entropy_mpw_generate_masterkey (self);
 
  /* read in sites from mpwsites */
  entropy_mpw_read_user_sites (self); 

  /* zero out password */
  memset ((void*)self->masterPassword, 0, strlen (self->masterPassword));
}

const int 
entropy_mpw_get_user_sites_count (EntropyMpwOperation *self)
{
  if (!self->user)
    return -1;
  
  MPMarshalledUser *user = self->user;
  const int sites_count = user->sites_count;

  return sites_count;
}

const gchar *
entropy_mpw_get_user_site_name (EntropyMpwOperation *self,
                                const int site_id)
{
  if (!self->user)
    return NULL;

  MPMarshalledUser *user = self->user;
  if (site_id >= user->sites_count)
    return NULL;

  MPMarshalledSite site = user->sites[site_id];

  const gchar *res = g_strdup (site.name);

  return res;
}

const gchar *
entropy_mpw_get_site_pw_placeholder (EntropyMpwOperation *self,
                                           const int site_id)
{

  if (!self->user)
    return NULL;
  
  MPMarshalledUser *user = self->user;
  MPMarshalledSite site = user->sites[site_id];
  MPResultType type = site.type;

  int n_characters = 0;
  switch (type)
  {
    case MPResultTypeTemplateMaximum:
      n_characters = 20;
      break;
    case MPResultTypeTemplateLong:
      n_characters = 14;
      break;
    case MPResultTypeTemplateMedium:
    case MPResultTypeTemplateBasic:
      n_characters = 8;
      break;
    case MPResultTypeTemplateShort:
    case MPResultTypeTemplatePIN:
      n_characters = 4;
      break;
    case MPResultTypeTemplateName:
      n_characters = 9;
      break;
    case MPResultTypeTemplatePhrase:
      n_characters = 18;
      break;
  }

  /* return a string with ● as placeholders */
  /* black dot: \u25CF : 0xE2 0x97 0x8F */
  char black_dot[3] = {-30, -105, -113};
  
  int buf_size = 3*n_characters + 1;
  char *buf = g_malloc (buf_size);

  for (int b = 0; b < buf_size - 1; ++b)
  {
    buf[b] = black_dot[b%3];
  }
  buf[buf_size - 1] = 0; // null-byte

  const gchar *ret = g_strdup (buf);
  g_free ((gpointer)buf);

  return ret;
}

const gchar *
entropy_mpw_get_site_pw (EntropyMpwOperation *self,
                               const int site_id)
{
  if (!self->user)
    return NULL;
  
  MPMarshalledUser *user = self->user;
  MPMarshalledSite site = user->sites[site_id];
  
  /*g_print ("generating site pw for %s: %i:%i:%i\n", site.name, site.type, site.algorithm, site.counter);*/

  const gchar *res = mpw_siteResult (self->masterKey,
                                     site.name,
                                     site.counter,
                                     self->keyPurpose,
                                     self->keyContext,
                                     site.type,
                                     self->resultParam,
                                     site.algorithm);
  return res;
}

void
entropy_mpw_set_site_counter (EntropyMpwOperation *self,
                              const int            site_id,
                              const int            counter)
{
  if (!self->user)
    return;
  
  MPMarshalledUser *user = self->user;
  MPMarshalledSite *site = &user->sites[site_id];

  site->counter = counter;
}

void
entropy_mpw_set_site_result_type (EntropyMpwOperation *self,
                                  const int site_id,
                                  const MPResultType result_type)
{
  if (!self->user)
    return;
  
  MPMarshalledUser *user = self->user;
  MPMarshalledSite *site = &user->sites[site_id];

  site->type = result_type;
}

void
entropy_mpw_set_site_algo_version (EntropyMpwOperation *self,
                                   const int site_id,
                                   const MPAlgorithmVersion algo_version)
{
  if (!self->user)
    return;
  
  MPMarshalledUser *user = self->user;
  MPMarshalledSite *site = &user->sites[site_id];

  site->algorithm = algo_version;
}

GObject *
entropy_mpw_new (void)
{
  EntropyMpwOperation *op;

  op = g_object_new (ENTROPY_TYPE_MPWOPERATION, NULL);

  return G_OBJECT (op);
}
