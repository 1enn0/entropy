# Entropy

Entropy is a native GTK+ application for the awesome [MasterPassword](https://gitlab.com/MasterPassword),
a stateless password management solution.

![Screenshot of a Entropy displaying saved sites for a user](docs/screenshots/screenshot_sites.png)

## Disclaimer

Entropy is in early development and not even close to stable / feature complete.
You have been warned ;)

## Dependencies

The core MasterPassword algorithm needs ```sodium``` and ```json-c```.

## Install

Entropy is built using the meson build system. Just run
```
meson --prefix=/usr build
ninja -C build
sudo ninja -C build install
```

## License

GPLv3, see [COPYING](COPYING).
